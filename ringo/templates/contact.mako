# -*- coding: utf8 -*-
<%inherit file="/main.mako" />
<div class="page-header">
<h1>${_('Contact')}</h1>
</div>
<address>
  <strong>Intevation GmbH</strong><br>
  ${_('E-Mail')}: <span class="muted">support@intevation.de</span><br>
</address>


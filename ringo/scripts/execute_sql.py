from ringo.scripts.db import get_engine


def handle_execute_sql_command(args):
    engine = get_engine(args.config)
    with open(args.file) as f, engine.connect() as con:
        con.execute(f.read())

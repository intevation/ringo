import pytest

from sqlalchemy.orm import Session
from sqlalchemy import engine_from_config
from argparse import Namespace

from ringo.scripts.db import handle_db_upgrade_command


@pytest.fixture(autouse=True, scope="session")
def setup_database(app_config):
    # Cleanup database to get rid of remains of previous tests.
    # Note that DROP OWNED is a PostgreSQL extension to the SQL standard.
    Db_session = Session(bind=engine_from_config(app_config), autocommit=True)
    Db_session.execute("DROP OWNED BY CURRENT_USER")

    # Initialize database
    handle_db_upgrade_command(Namespace(
        app = app_config.context.distribution.project_name,
        config = app_config.context.loader.filename))

    return

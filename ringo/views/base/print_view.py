from ringo.views.helpers import get_item_form, render_item_form
from ringo.views.request import (
    get_return_value,
    handle_callback,
    handle_params,
)


def printview(request, callback=None, renderers=None):
    """Base method to handle read requests. Returns a dictionary of
    values used available in the rendererd template The template to
    render is defined in the view configuration.

    :request: Current request
    :callback: Current function which is called after the item has been read.
    :returns: Dictionary.
    """
    handle_params(request)
    handle_callback(request, callback, mode="pre,default")
    rvalues = get_return_value(request)
    values = {"_roles": [str(r.name) for r in request.user.roles]}
    form = get_item_form("print", request, renderers, values=values)
    rvalues["form"] = render_item_form(request, form)
    handle_callback(request, callback, mode="post")
    return rvalues

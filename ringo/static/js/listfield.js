function checkAll(checkId) {
  $("input[name='" + checkId + "'][type='checkbox']").each((_, e) => {
    if (e.classList.contains("hidden")) return;
    e.checked = document.querySelector("input[name='check_all']").checked;
  });
  check(checkId);
}

function checkOne(checkId, element) {
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < inputs.length; i++) {
    if (
      inputs[i].type == "checkbox" &&
      inputs[i].name == checkId &&
      inputs[i].value != element.value
    ) {
      inputs[i].checked = false;
    }
  }
  check(checkId);
}

function check(checkId, element) {
  /* Will add a hidden checkbox with no value in case no other checkbox is
   * selected. This is needed to items with no selection, as in this case html
   * does not submit the checkbox field at all. So this is a hack to simulate
   * an empty selection */
  var inputs = $("input[type='checkbox'][name='" + checkId + "']");
  var noSelection =
    $("input[type='checkbox'][name='" + checkId + "']:checked").length === 0;
  if (noSelection) {
    $(inputs[0]).after(
      '<input id="' +
        checkId +
        '-empty" style="display:none" type="checkbox" value="" name="' +
        checkId +
        '" checked="checked"/>'
    );
  } else {
    $("#" + checkId + "-empty").remove();
  }
}
